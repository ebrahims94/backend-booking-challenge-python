#!/bin/bash

# Apply database migrations
alembic upgrade head

# Start your FastAPI application
uvicorn main:app --host 0.0.0.0 --port 8000 --workers 1 --reload
