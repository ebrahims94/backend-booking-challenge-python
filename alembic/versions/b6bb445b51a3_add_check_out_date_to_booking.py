"""add check out date to booking

Revision ID: b6bb445b51a3
Revises: 
Create Date: 2023-09-11 19:45:36.702995

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b6bb445b51a3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table('booking',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('guest_name', sa.VARCHAR(), nullable=False),
    sa.Column('unit_id', sa.VARCHAR(), nullable=False),
    sa.Column('check_in_date', sa.DATE(), nullable=False),
    sa.Column('number_of_nights', sa.INTEGER(), nullable=False),
    sa.Column('check_out_date', sa.DATE(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_booking_id', 'booking', ['id'], unique=False)


def downgrade() -> None:
    op.drop_index('ix_booking_id', table_name='booking')
    op.drop_table('booking')
