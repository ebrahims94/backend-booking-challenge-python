FROM python:3.9
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
#COPY . /code/app
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app
# COPY start.sh /app/start.sh
# RUN chmod +x /app/start.sh
# CMD ["/app/start.sh"]
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000", "--workers", "1", "--reload"]