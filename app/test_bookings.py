import datetime

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.database import Base
from app.main import app, get_db

SQLALCHEMY_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)

TODAY_DATE = datetime.date.today()

GUEST_A_UNIT_1: dict = {
    'unit_id': '1', 'guest_name': 'GuestA', 'check_in_date': TODAY_DATE.strftime('%Y-%m-%d'),
    'number_of_nights': 5,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d')
}
GUEST_A_UNIT_2: dict = {
    'unit_id': '2', 'guest_name': 'GuestA', 'check_in_date': TODAY_DATE.strftime('%Y-%m-%d'),
    'number_of_nights': 5,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d')
}
GUEST_B_UNIT_1: dict = {
    'unit_id': '1', 'guest_name': 'GuestB', 'check_in_date': TODAY_DATE.strftime('%Y-%m-%d'),
    'number_of_nights': 5,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d')
}
GUEST_B_UNIT_1_WIDE_INTERVAL: dict = {
    'unit_id': '1', 'guest_name': 'GuestB', 'check_in_date': (TODAY_DATE - datetime.timedelta(days=3)).strftime('%Y-%m-%d'),
    'number_of_nights': 8,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d')
}
GUEST_A_UNIT_1_EXTEND_UNIT_1: dict = {
    'unit_id': '1', 'guest_name': 'GuestA', 'check_in_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d'),
    'number_of_nights': 3,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=8)).strftime('%Y-%m-%d')
}
GUEST_A_UNIT_1_EXTEND_UNIT_2: dict = {
    'unit_id': '2', 'guest_name': 'GuestA', 'check_in_date': (TODAY_DATE + datetime.timedelta(days=5)).strftime('%Y-%m-%d'),
    'number_of_nights': 3,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=8)).strftime('%Y-%m-%d')
}
GUEST_A_UNIT_1_EXTEND_GAP: dict = {
    'unit_id': '1', 'guest_name': 'GuestA', 'check_in_date': (TODAY_DATE + datetime.timedelta(days=6)).strftime('%Y-%m-%d'),
    'number_of_nights': 3,
    'check_out_date': (TODAY_DATE + datetime.timedelta(days=9)).strftime('%Y-%m-%d')
}


@pytest.fixture()
def test_db():
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.mark.freeze_time('2023-09-13')
def test_create_fresh_booking(test_db):
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    response.raise_for_status()
    assert response.status_code == 200, response.text


@pytest.mark.freeze_time('2023-09-13')
def test_same_guest_same_unit_booking(test_db):
    # Create first booking
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 200, response.text
    response.raise_for_status()

    # Guests want to book same unit again
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'The given guest name cannot book the same unit multiple times.'


@pytest.mark.freeze_time('2023-09-13')
def test_same_guest_different_unit_booking(test_db):
    # Create first booking
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 200, response.text

    # Guest wants to book another unit
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_2
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'The same guest cannot be in multiple units at the same time.'


@pytest.mark.freeze_time('2023-09-13')
def test_different_guest_same_unit_booking(test_db):
    # Create first booking
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 200, response.text

    # GuestB trying to book a unit that is already occuppied
    response = client.post(
        "/api/v1/booking",
        json=GUEST_B_UNIT_1
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'For the given check-in date, the unit is already occupied.'


@pytest.mark.freeze_time('2023-09-13')
def test_different_guest_same_unit_booking_with_wider_date_interval(test_db):
    # Create first booking
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 200, response.text

    # GuestB trying to book a unit that is already occuppied
    response = client.post(
        "/api/v1/booking",
        json=GUEST_B_UNIT_1_WIDE_INTERVAL
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'For the given check-in date, the unit is already occupied.'


@pytest.mark.freeze_time('2023-09-13')
def test_different_guest_same_unit_booking_different_date(test_db):
    # Create first booking
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    assert response.status_code == 200, response.text
    print("response.text :", response.text)

    # GuestB trying to book a unit that is already occuppied
    response = client.post(
        "/api/v1/booking",
        json={
            'unit_id': '1',  # same unit
            'guest_name': 'GuestB',  # different guest
            # check_in date of GUEST A + 1, the unit is already booked on this date
            'check_in_date': (datetime.date.today() + datetime.timedelta(1)).strftime('%Y-%m-%d'),
            'number_of_nights': 5,
            'check_out_date': (datetime.date.today() + datetime.timedelta(6)).strftime('%Y-%m-%d'),
        }
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'For the given check-in date, the unit is already occupied.'


@pytest.mark.freeze_time('2023-09-13')
def test_extend_existing_booking(test_db):
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    response.raise_for_status()
    assert response.status_code == 200, response.text

    extend_booking_response = client.patch(
        "/api/v1/extend-booking",
        json=GUEST_A_UNIT_1_EXTEND_UNIT_1
    )
    extend_booking_response.raise_for_status()
    assert extend_booking_response.status_code == 200, extend_booking_response.text


@pytest.mark.freeze_time('2023-09-13')
def test_extend_existing_booking_different_unit(test_db):
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    response.raise_for_status()
    assert response.status_code == 200, response.text

    extend_booking_response = client.patch(
        "/api/v1/extend-booking",
        json=GUEST_A_UNIT_1_EXTEND_UNIT_2
    )
    assert extend_booking_response.status_code == 400, extend_booking_response.text
    assert extend_booking_response.json()['detail'] == 'There is no booking to extend for you.'


@pytest.mark.freeze_time('2023-09-13')
def test_extend_existing_booking_invalid_interval(test_db):
    """
    it is not going to start from the checkout date there is a gap.
    """
    response = client.post(
        "/api/v1/booking",
        json=GUEST_A_UNIT_1
    )
    response.raise_for_status()
    assert response.status_code == 200, response.text

    extend_booking_response = client.patch(
        "/api/v1/extend-booking",
        json=GUEST_A_UNIT_1_EXTEND_UNIT_1
    )
    extend_booking_response.raise_for_status()
    assert extend_booking_response.status_code == 200, extend_booking_response.text


@pytest.mark.freeze_time('2023-09-13')
def test_extend_without_existing_booking(test_db):
    response = client.patch(
        "/api/v1/extend-booking",
        json=GUEST_A_UNIT_1_EXTEND_UNIT_1
    )
    assert response.status_code == 400, response.text
    assert response.json()['detail'] == 'There is no booking to extend for you.'
