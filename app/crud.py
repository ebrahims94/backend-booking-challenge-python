from datetime import timedelta
from typing import Tuple, Union
from sqlalchemy import or_, and_
from sqlalchemy.orm import Session

from . import models, schemas


class UnableToBook(Exception):
    pass


def create_booking(db: Session, booking: schemas.BookingBase) -> models.Booking:
    (is_possible, reason) = is_booking_possible(db=db, booking=booking)
    if not is_possible:
        raise UnableToBook(reason)
    db_booking = models.Booking(
        guest_name=booking.guest_name, unit_id=booking.unit_id,
        check_in_date=booking.check_in_date, number_of_nights=booking.number_of_nights,
        check_out_date=booking.check_out_date)
    db.add(db_booking)
    db.commit()
    db.refresh(db_booking)
    return db_booking


def extend_booking(db: Session, booking: schemas.BookingBase) -> models.Booking:
    """
    This function is used to extend the user current booking for the same property
    eg: user booked 3 nights and wanted to extend more 2 nights
    # of nights = old value + new value = 5
    checkout date will be last day to check out not old one
    """
    # check user already have a booking for this property
    current_booking = db.query(models.Booking) \
        .filter_by(guest_name=booking.guest_name, unit_id=booking.unit_id).first()

    if not current_booking:
        raise UnableToBook("There is no booking to extend for you.")
    
    if current_booking.check_out_date != booking.check_in_date:
        raise UnableToBook("Please check dates again.")
    
    # check if there is any check in or checkout (booked) in the stay period
    overlapped_booking = get_overlapped_booking(db=db, booking=booking)
    
    if overlapped_booking:
        raise UnableToBook("For the given check-in date, the unit is already occupied.")

    current_booking.check_out_date += timedelta(days=booking.number_of_nights)
    current_booking.number_of_nights += booking.number_of_nights 
    db.add(current_booking)
    db.commit()
    db.refresh(current_booking)
    return current_booking


def get_overlapped_booking(db: Session, booking: schemas.BookingBase) -> Union[models.Booking, None]:
    """
    This function to check if there is any existing booking overlapped with the requested booking
    to avoid over booking the properties
    """
    return db.query(models.Booking).filter_by(unit_id=booking.unit_id) \
        .filter(
            models.Booking.check_in_date < booking.check_out_date,
            models.Booking.check_out_date > booking.check_in_date
        ).first()


def is_booking_possible(db: Session, booking: schemas.BookingBase) -> Tuple[bool, str]:
    # check 1 : The Same guest cannot book the same unit multiple times
    is_same_guest_booking_same_unit = db.query(models.Booking) \
        .filter_by(guest_name=booking.guest_name, unit_id=booking.unit_id).first()

    if is_same_guest_booking_same_unit:
        return False, 'The given guest name cannot book the same unit multiple times.'

    # check 2 : the same guest cannot be in multiple units at the same time
    is_same_guest_already_booked = db.query(models.Booking) \
        .filter_by(guest_name=booking.guest_name).first()
    if is_same_guest_already_booked:
        return False, 'The same guest cannot be in multiple units at the same time.'

    # check 3 : check if there is any check in or checkout (booked) in the stay period
    overlapped_booking = get_overlapped_booking(db=db, booking=booking)

    if overlapped_booking:
        return False, 'For the given check-in date, the unit is already occupied.'

    return True, 'OK'
