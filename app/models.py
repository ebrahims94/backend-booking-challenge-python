from sqlalchemy import Column, Integer, String, Date

from .database import Base


class Booking(Base):
    __tablename__ = "booking"

    # all of those fields can't be null
    id = Column(Integer, primary_key=True, index=True)
    guest_name = Column(String, nullable=False)
    unit_id = Column(String, nullable=False)
    check_in_date = Column(Date, nullable=False)
    number_of_nights = Column(Integer, nullable=False)
    # added this new field to make the check for validation easier
    check_out_date = Column(Date, nullable=False)
